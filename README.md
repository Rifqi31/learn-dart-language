### Learn Dart Language

This repository is for saving my learning in a dart programming language, exercises, concepts programming, and snippets code.

There are 4 folders: 
* fundamental
* exercises
* snippets
* concepts

`fundamental` folder containing for fundamental files, `exercises` folder is for containing fundamental language, `snippets` folder if there's good stuff on my projects like important function or class or another implementation with dart and flutter, `concepts` folder is very different, this contains concept programming in generals but using dart language.