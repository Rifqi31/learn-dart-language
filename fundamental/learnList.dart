void main () {
	// fixed length list
	var testList = new List(3);
	testList[0] = 12;
	testList[1] = 13;
	testList[2] = 14;

	// ordinary list
	// manipulate list
	var data = [10, 20, 30];
	// insert a list
	// add value to the list
	data.insert(0, 100);		// param index, value
	data.add(200);			// param value
	data.addAll([300, 400]);	// param add all value to the list
	data.insertAll(3, [120, 120]);	// param index, list value
	// result
	print("here from fixed list = ${testList}");
	print("here from example insert value to the list = ${data}");

	var data2 = [100, 200, 300];
	// update a list
	data2[0] = 120;						// modify specific index value
	data2.replaceRange(0, 1, [500, 600, 100, 900]);		// modify specific index range value
	// result
	print("here from example update value to the list = ${data2}");
	
	var data3 = ["Biology", "Physic", "Chemistry", "History", "Mathematic"];
	// result
	print("origin data collection = ${data3}");
	// bool res = data3.remove("Biology"); 			// remove by element
	int lengthData = data3.length;
	// String res2 = data3.removeAt(lengthData - 1);	// remove by index
	// dynamic res = data3.removeLast();			// remove last element on the list
	data3.removeRange(1, lengthData);			// remove element by range on the list
	// result after remove
	// print(lengthData);
	print("example remove function = ${data3}");
}
