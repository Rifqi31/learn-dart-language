// reference function
bool predicate(String element) {
  return element.length > 5;
}

void main() {
  // -- Reading Elements --
  var food = ['Salad', 'Popcorn', 'Toast'];
  // pick one element from list
  String value = food.elementAt(0);
  print("this is from elementAt function = $value");
  // pick all using for in function
  for (var element in food) {
    print(element);
  }
  // pick first and last element
  print("first element from food = ${food.first}");
  print("last element from food = ${food.last}");

  // -- Checking Conditions --
  // implement firstWhere()
  // simple expression :
  var simpleResult = food.firstWhere((food) => food.length > 5);
  print(simpleResult);
  // or try using function block
  var blockResult = food.firstWhere((food) {
    return food.length > 5;
  });
  print(blockResult);
  // or even pass in a function reference
  var refResult = food.firstWhere(predicate);
  print(refResult);
  // implement 'orElse' function
  var orElseResult = food.firstWhere(
    (food) => food.length > 10,
    orElse: () => 'None!',
  );
  print(orElseResult);

  // implement any() and every()
  // any() Returns true if at least one element satisfies the condition.
  // every() Returns true if all elements satisfy the condition.
  if (food.any((food) => food.contains('a'))) {
    print('At least one element contains "a" ');
  }

  if (food.every((food) => food.length >= 5)) {
    print('All elements have length >= 5');
  }

  // -- Filtering -- 
  // -- Mapping
}
