void main () {
  // everything are object
  // variable can be null if their doesn't have value or defaul are null
  int nullNumber;
  print(nullNumber);

  // dynamic keyword
  // same as var or let in javascript can be replace by another value
  dynamic petAnimal = "cat";
  print(petAnimal);

  // const and final
  // const are data type for compile-time which means value can be change when program was compiled
  // final are data type for not to be process, which mean can't be change by any other variable or function, but only for help procesing
  const categoryId = 12;
  final birthDay = "31/01/1996";
  print(categoryId);
  print(birthDay);
}