void createOrderMessage() async {
  print('Awaiting user order ...');
  var order = await fetchUserOrder(); // wait this process complete then print order
  print('You order is : $order');
}

// handling error
void printOrderMessage() async{
  try {
    var order = await fetchUserOrder();
    print("Awaiting user order ...");
    print(order);
  } catch (error) {
    print("Caught error: $error");
  }
}

Future<String> fetchUserOrder() {
  // imagine that function is more complex and slow
  // return Future.delayed(Duration(seconds: value), () => 'Large Latte' ); // arrow function mean auto return
  var result = Future.delayed(Duration(seconds: 4), () => throw 'Cannot locate user order' ); // arrow function mean auto return
  return result;
}

Future<void>main() async {
  // int delaySeconds = 4;
  // countSecond(delaySeconds); // visualize a delay
  // await createOrderMessage(delaySeconds); // run slowly depends on the delay
  await printOrderMessage();
}

// visualize delay time
void countSecond(value) {
  for (var i = 1; i <= value; i++) {
    Future.delayed(Duration(seconds: i), () => print(i));
  }
}
