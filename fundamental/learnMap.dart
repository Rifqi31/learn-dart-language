void main() {
  // simple identifier map
  // example map constructor
  var indentifierMap = new Map(); // indetfier new map
  indentifierMap['weapon_name'] = 'AK-47';
  indentifierMap['weapon_type'] = 'Assault Riffle';
  // print out the value
  print(indentifierMap);

  // direct creating new map with value
  var details = {'player_name': 'Gedion', 'max_health': 100};
  // old value = {player_name: Gedion, max_health: 100}
  // add value to map
  details['point'] = 0;
  print(details);

  // print length of the map
  print(details.length);
  // print is empty or not
  // print(details.isEmpty);
  if (details.isEmpty == true) {
    print("object are empty");
  } else {
    print("object aren't empty");
  }

  // manipulate the map
  var characterInit = new Map();
  characterInit.addAll({
    'name': 'Ori',
    'type': 'Main Character',
    'power': 'Obstacle'
  }); // add object into new map
  // old value = {name: Ori, type: Main Character, power: Obstacle}
  characterInit.remove('power'); // remove specific key in the map
  // Checks whether the given key exists
  if (characterInit.containsKey('power') == true) {
    print("character has a power");
  } else {
    print("character hasn't a power");
  }

  // Checks whether the given value exists
  if (characterInit.containsValue('Ori') == true) {
    print("character has a name\n");
  } else {
    print("character hasn't a name\n");
  }

  // update value
  // old value
  //  'name': 'Ori',
  //  'type': 'Main Character',
  //  'power': 'Obstacle'
  characterInit.update("type", (string) => 'Main Protagonist');
  // update character map with power key, but if power key doesn't exist then using ifAbsent() function to prevent certain key are missing
  characterInit.update("power", (string) => 'Obstacle Terrain',
      ifAbsent: () => 'Obstacle');

  // create duplicate map
  var characterInitCopy = Map.from(characterInit);
  characterInitCopy.update("name", (string) => 'Ku');
  characterInitCopy.update("power", (string) => 'Obstacle Terrain and Flying');

  Map kidsBooks = {
    'Matilda': 'Roald Dahl',
    'Green Eggs and Ham': 'Dr Seuss',
    'Where the Wild Things Are': 'Maurice Sendak'
  };
  for (var book in kidsBooks.keys) {
    print('$book was written by ${kidsBooks[book]}');
  }
  print('\n');

  // print out value without brackets
  characterInit.forEach((key, value) => {print('$key : $value')});
  print('\n');
  characterInitCopy.forEach((key, value) => {print('$key : $value')});
  print('\n');

  // Adds a key/value pair if non-existent. If key already exists, a value will be set if there isn’t one
  var user = new Map();
  var detailsUser = {
    "first_name": 'Sabrina',
    "last_name": 'Abi Saab',
    //"access_token": 'awduawygdwaudgwauydgawudgawdgajdvaywyatfajddvakuyd'
  };
  user.addAll(detailsUser);
  user.putIfAbsent("access_token",
      () => 'awduawygdwaudgwauydgawudgawdgajdvaywyatfajddvakuyd');

  user.removeWhere((key, value) =>
      key ==
      'last_name'); // Removes the key/value pair if the given condition is true
  user.forEach((key, value) => print('$key : $value'));
}
