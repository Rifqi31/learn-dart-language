void main() {
  // same as javascript or another language
  // so in this code just have a little example of it
  var number1 = 10;
  var number2 = 100;

  var result = number2 > number1
      ? "number ${number2} has a greater than number ${number1}\n"
      : "number ${number1} has a greater than number ${number2}\n";
  print(result);

  var emptyObj = null;
  var compareNull = result ?? emptyObj;
  print("compare number with null object = ${compareNull}");

  final pi = 3.14; // really final number cant be change in compiler
  var anotherNumber = 27;

  var count = anotherNumber * pi;
  print("the result is = ${count}");
}
