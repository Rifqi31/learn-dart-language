void main() {
  // learn looping in dart language
  // definite/fixed loop
  var valueTest = new List();
  valueTest.addAll(
      ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Friday', 'Saturday']);
  // use fixed loop
  // for .. loop
  for (var valueIndex = 0; valueIndex < valueTest.length; valueIndex++) {
    print(valueTest[valueIndex]);
  }
  print('\n');
  // for in .. lop
  for (var valueIndex in valueTest) {
    print(valueIndex);
  }
  print('\n');

  // `for in loop` can use for map too
  var valueTestMap = new Map();
  valueTestMap.addAll({
    'player_name': 'gedion',
    'live_point': 3,
    'max_health': 100,
    'current_health': 40,
    'ability': 'Obstacle Terran'
  });
  // example to print all keys object
  for (var valueObject in valueTestMap.keys) {
    print(valueObject);
  }
  print('\n');
  // example to print all values object
  for (var valueObject in valueTestMap.values) {
    print(valueObject);
  }
  print('\n');
  // for show both keys and values
  valueTestMap.forEach((key, value) => print('$key\t: $value'));

  // indefinite loops
  // which mean number of iterations in a loop is indeterminate or unknown
  // while loop
  print('\n');
  var indexWhile = 0;
  var newDays = new List();
  bool signal = false;
  print('remove last value using break statement');
  while (indexWhile < valueTest.length) {
    if (valueTest[indexWhile] == 'Saturday') {
      signal = true;
      break;
    } else {
      print(valueTest[indexWhile]);
    }
    indexWhile++;
  }
  print('\n');
  print('remove last value using removeLast() function');
  if (signal == true) {
    newDays.addAll(valueTest);
    if (newDays.length != 0) {
      newDays.removeLast();
    }
  }
  for (var value in newDays) print(value);

  // do ..  while loop
  // the code block will be executed at least once in a do…while loop
  print('\n');
  var number = 10;
  do {
    print(number);
    number--;
  } while (number >= 0);
}