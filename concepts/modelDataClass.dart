void main(List<String> args) {
  // variable list with instance model data class
  var exampleDataList = new List<MovieItemModel>();
  // add each data
  exampleDataList.add(MovieItemModel(1, "https://picsum.photos/200", "Example Item 1"));
  exampleDataList.add(MovieItemModel(2, "https://picsum.photos/200", "Example Item 2"));
  exampleDataList.add(MovieItemModel(3, "https://picsum.photos/200", "Example Item 3"));
  exampleDataList.add(MovieItemModel(4, "https://picsum.photos/200", "Example Item 4"));
  
  // for the test result
  // for (var index = 0; index < exampleDataList.length; index++) {
  //   print(exampleDataList[index]._itemName); // references to _itemName from model class
  // }

  // for in loop
  for (var element in exampleDataList) {
    print(element.getItemName()); // references to getItemName() from model class
    // result
    // Example Item 1
    // Example Item 2
    // Example Item 3
    // Example Item 4
  }
}

// model data class
class MovieItemModel {
  // private variables starts with _ underscores
  int _itemId;
  String _itemImageUrl;
  String _itemName;

  // public construtor
  MovieItemModel(int paramItemId, String paramItemImageUrl, String paramItemName) {
    this._itemId = paramItemId;
    this._itemImageUrl = paramItemImageUrl;
    this._itemName = paramItemName;
  }

  // return private variable
  // return item id
  int getItemId() {
    return _itemId;
  }

  // return item image url
  String getItemImageUrl() {
    return _itemImageUrl;
  }

  // return item name
  String getItemName() {
    return _itemName;
  }
}