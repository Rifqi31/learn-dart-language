// contain service url with parameter url name, and body
String urlApi(urlName, body) {
  // init variable
  final String apikey = '9a4a662a126525b07d4b84b079d809d8';
  final String serverUrl = 'https://api.themoviedb.org/3/';
  var containKeyAndValue = new List<String>(); // empty List
  var resultValueBuffer = StringBuffer(); // string buffer

  // read the param and add into List
  body.forEach((key, value) {
    containKeyAndValue.add('$key$value');
  });

  // read the List and conver into one line string buffer
  containKeyAndValue.forEach((item) {
    resultValueBuffer.write(item);
  });

  // contain and convert from string buffer into string
  var bodyResult = resultValueBuffer.toString();

  // as the result add result api param into body server url
  Map<String, String> containUrl = {
    "movieDiscover":
        serverUrl + "discover/movie" + "?api_key=" + apikey + bodyResult
  };

  return containUrl[urlName];
}

// this function for add character '&' and '=' into key
// and convert value into string data type
Map<String, dynamic> changeMapKey(param) {
  var newParamBody = new Map<String, dynamic>();
  param.forEach((key, value) {
    if (value.runtimeType == bool || value.runtimeType == int) {
      newParamBody['&' + '$key' + '='] = value.toString();
    } else {
      newParamBody['&' + '$key' + '='] = value;
    }
  });

  return newParamBody;
}

void main() {
  // old map while user config
  Map<String, dynamic> paramBody = {
    'language': 'en-US',
    'sort_by': 'popularity.desc',
    'include_adult': true,
    'include_video': false,
    'page': 1
  };

  // call converter map key and value
  var resultBody = changeMapKey(paramBody);
  // call url service
  var resultUrl = urlApi("movieDiscover", resultBody);
  print(resultUrl);
}
