void userLoginPost() async {
  // String username = null;
  // String password = null;
  String username = 'nomad123';
  String password = 'test1234pulsar';

  try {
    print("request user login post ...");
    print("username = $username");
    print("password = $password");
    var request = await fetchUserLogin(username, password);
    print(request);
  } catch (error) {
    print("Caught error : $error");
  }
}

Future<String> fetchUserLogin(username, password) {
  var result = Future.delayed(Duration(seconds: 4), () {
    if (username == null && password == null || username.length == 0 && password.length == 0) {
      return throw 'User are not existed';
    } else {
      return 'User Logged';
    }
  });

  return result;
}

Future<void> callFuncPost() async {
  await userLoginPost();
}

// visualize delay time
void countSecond(value) {
  for (var i = 1; i <= value; i++) {
    Future.delayed(Duration(seconds: i), () => print(i));
  }
}

void main() {
  countSecond(4);
  callFuncPost();
}